import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import Stack from "./src/route/stack";

const App = () => {
  return (
    <NavigationContainer>
        <Stack />
    </NavigationContainer>
  )
}

export default App;