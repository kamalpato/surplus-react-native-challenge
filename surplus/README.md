CREDENTIAL:

Username: kamaldotpato@gmail.com
Password: 123456
Device Test: Iphone XR, Iphone 14 Pro, Xiaomi Redmi 10s

Installation Guide for React Native App

Prerequisites:

Node.js and npm installed on your machine
A code editor of your choice (e.g., Visual Studio Code)
React Native CLI installed globally on your machine
To install React Native CLI, run the following command in the terminal:
npm install -g react-native-cli
Cloning the Repository:

Clone the repository to your local machine using Git:
git clone https://github.com/{username}/{repo-name}.git
Change into the project directory:
cd {repo-name}
Installing Dependencies:

Install the required npm packages:
npm install
Running the App:

Run the React Native app on an Android emulator:
react-native run-android
or
Run the React Native app on an iOS simulator:
react-native run-ios
Troubleshooting:
If you run into any issues during the installation, please make sure that you have followed all the steps correctly. If the problem persists, feel free to raise an issue in the repository's issue tracker.
