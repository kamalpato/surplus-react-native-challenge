import React from "react";
import  { createNativeStackNavigator }  from "@react-navigation/native-stack";

import ViewSplash from "../screen/auth/splash/ViewSplash";
import ViewIntro from "../screen/auth/intro/ViewIntro";
import ViewLogin from "../screen/auth/login/ViewLogin";
import TabRoute from "./tab";

const Stack = createNativeStackNavigator();

const StackScreen = () => {
    return (
        <Stack.Navigator initialRouteName="ViewSplash" >
            <Stack.Screen name='ViewSplash' component={ViewSplash} options={{ headerShown: false }} />
            <Stack.Screen name='ViewIntro' component={ViewIntro} options={{ headerShown: false }} />
            <Stack.Screen name='ViewLogin' component={ViewLogin} options={{ headerShown: false }} />
            <Stack.Screen name='TabRoute' component={TabRoute} options={{ headerShown: false }} />
        </Stack.Navigator>
    )
}

export default StackScreen;