import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TouchableOpacity, Text, Image, StyleSheet } from 'react-native';

const Tab = createBottomTabNavigator();

import ViewProfile from '../screen/tab/profile/ViewProfile';
const Icon = require('../assets/SettingIcon.png');
const Profil = require('../assets/Profile.png');

const TabRoute = () => {
    return (
        <Tab.Navigator >
            <Tab.Screen name='Profil' component={ViewProfile} options={{
                headerRight: () => (
                    <TouchableOpacity>
                        <Image source={Icon} style={Sytles.Icon} />
                    </TouchableOpacity>
                ),
                tabBarIcon: () => (
                    <Image source={Profil} style={{ height: 25, width: 25 }} />
                ),
                tabBarLabelStyle: { color: "#A8A8A8" },
            }} />
        </Tab.Navigator>
    )
}
export default TabRoute;

const Sytles = StyleSheet.create({
    Icon: {
        height: 35,
        width: 35,
        marginRight: 10
    },
})