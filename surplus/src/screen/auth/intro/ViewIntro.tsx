import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";

const ShopAnimated = require("../../../assets/ShopAnimated.jpg")

const ViewIntro = () => {
    const Navigation = useNavigation()
    return (
        <View style={Styles.ContainerInner}>
            {/* Mark: Skip Button */}
            <View style={Styles.SkipContainer}>
                <TouchableOpacity onPress={() => Navigation.navigate("TabRoute")} style={Styles.SkipButton}>
                    <Text style={Styles.SkipLabel}>Lewati</Text>
                </TouchableOpacity>
            </View>
            {/* Mark: Animated Picture */}
            <View style={Styles.MainLogoContainer}>
                <Image source={ShopAnimated} style={Styles.MainLogo} />
            </View>
            {/* Mark: Action Area */}
            <View style={Styles.WelcomeContainer}>
                {/* Mark: Welcome Text */}
                <View style={Styles.ContainerWelcome}>
                    <View style={Styles.ContainerWelcomeText}>
                        <Text style={Styles.WelcomeText}>Selamat datang di Surplus</Text>
                    </View>
                    <Text style={Styles.SloganText}>Selamatkan makanan berlebih di aplikasi</Text>
                    <Text style={Styles.SloganText}>Surplus agar tidak terbuang sia-sia</Text>
                </View>
                {/* Mark: Action Auth Button */}
                <View style={Styles.ActionAuthButton}>
                    <TouchableOpacity style={Styles.RegisterButton}>
                        <Text style={Styles.RegisterText}>Daftar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Navigation.navigate("ViewLogin")} style={Styles.LoginButton}>
                        <Text style={Styles.LoginText}>Sudah punya akun? Masuk</Text>
                    </TouchableOpacity>
                </View>
                {/* Mark: TnC  */}
                <View style={Styles.Terms}>
                    <Text style={Styles.SloganText}>Dengan daftar atau masuk, Anda menerima
                        <Text style={Styles.TermsPrivacy}> syarat dan ketentuan</Text>
                    </Text>
                    <Text style={Styles.SloganText}>Surplus agar tidak terbuang sia-sia
                        <Text style={Styles.TermsPrivacy}> kebijakan privasi</Text>
                    </Text>
                </View>
            </View>
        </View>
    )
}
export default ViewIntro;

const Styles = StyleSheet.create({
    ContainerInner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#B5F0FE'
    },
    MainLogoContainer: {
        flex: .5,
        alignItems: 'center',
    },
    SkipContainer: {
        flex: .2,
        width: "100%",
        justifyContent: 'center',
        marginRight: 50,
        alignItems: 'flex-end'
    },
    SkipButton: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderWidth: 1,
        borderRadius: 20,
        borderColor: '#51B8A7',
    },
    SkipLabel: {
        fontWeight: 'bold',
        color: '#51B8A7'
    },
    MainLogo: {
        resizeMode: "contain",
        height: "60%",
        position: "relative",
    },
    WelcomeContainer: {
        flex: .6,
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        backgroundColor: '#FFF'
    },
    ContainerWelcome: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ContainerWelcomeText: {
        height: 40
    },
    WelcomeText: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    ActionAuthButton: {
        flex: 1,
        width: "100%",
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    RegisterButton: {
        height: 45,
        width: "90%",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: '#009688'
    },
    RegisterText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    LoginButton: {
        height: 45,
        width: "90%",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: "#009688"
    },
    LoginText: {
        color: '#009688',
        fontWeight: 'bold'
    },
    Terms: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    SloganText: {
        fontSize: 12,
        color: "#A3A3A3"
    },
    TermsPrivacy: {
        color: "#ECBB5D"
    }
})
