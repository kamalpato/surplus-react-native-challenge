import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { Alert, Image, ImageBackground, KeyboardAvoidingView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";

const FoodAnimated = require("../../../assets/FoodAnimated.png")
const EyeActive = require("../../../assets/Eye-On.png")
const EyeDeActive = require("../../../assets/Eye-Off.png")
const FacebookLogo = require("../../../assets/Facebook.png")
const GoogleLogo = require("../../../assets/Google.png")

const ViewIntro = () => {
  const Navigation = useNavigation();
  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setUserPassword] = useState("")
  const [hide, setHide] = useState(false)
  return (
    <View style={Styles.ContainerInner}>
      {/* Mark: Animated Picture */}
      <View style={Styles.ImageContainer}>
        <Image source={FoodAnimated} style={Styles.MainLogo} />
        <View style={Styles.LoginTextContainer}>
          <Text style={Styles.LoginTextLabel}>{`Masuk\n`}
            <Text style={Styles.LoginTextSubLabel}>{`Pastikan kamu sudah pernah\n`}</Text>
            <Text style={Styles.LoginTextSubLabel}>membuat akun Surplus</Text>
          </Text>
        </View>
      </View>
      {/* Mark: Title Wording */}
      <View style={Styles.MainContainer}>
        <ScrollView style={Styles.ScrollContainer}>
          {/* Mark: Action Area */}
          <View style={Styles.WelcomeContainer}>
            {/* Mark: Input Area */}
            <View style={Styles.EmailInputContainer}>
              <Text style={Styles.InputLabel}>E-mail</Text>
              <KeyboardAvoidingView style={Styles.AvoidingKeyboard} behavior="padding">
                <View style={Styles.EmailInput}>
                  <TextInput autoCapitalize={false} style={{ textAlignVertical: 'center' }} value={userEmail} onChangeText={(e) => setUserEmail(e)} />
                </View>
              </KeyboardAvoidingView>
            </View>
            <View style={Styles.PasswordInputContainer}>
              <Text style={Styles.InputLabel}>Kata Sandi</Text>
              <View style={Styles.PasswordInputContent}>
                <TextInput autoCapitalize={false} style={{ width: "80%", textAlignVertical: 'center' }} secureTextEntry={!hide} value={userPassword} onChangeText={(e) => setUserPassword(e)} />
                <TouchableOpacity onPress={()=>setHide(!hide)} style={{ width: "20%", paddingRight: 20, justifyContent: 'center', alignItems: 'flex-end' }}>
                  <Image source={hide ? EyeActive : EyeDeActive} style={{ height: 25, width: 25 }} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={Styles.ForgotPassContainer}>
              <Text style={Styles.ForgotPassword}>Lupa Kata Sandi?</Text>
            </View>

          </View>

          <View style={Styles.FarewellContainer}>
            {/* Mark: Login Button */}
            <View style={Styles.ActionAuthButton}>
              <TouchableOpacity onPress={() => {
                if (userEmail == "kamaldotpato@gmail.com" && userPassword == "123456") {
                  Alert.alert("Succees", "You have logged in", [
                    { text: 'OK', onPress: () => Navigation.navigate("TabRoute") }
                  ])
                } else {
                  Alert.alert("Failed", "Check your password or email", [
                    { text: 'OK', }
                  ])
                }
              }
              } style={Styles.RegisterButton}>
                <Text style={Styles.RegisterText}>Masuk</Text>
              </TouchableOpacity>
            </View>
            {/* Mark: Spacer */}
            <View style={Styles.SpacerContainer}>
              <View style={Styles.SpacerLeft} />
              <Text style={Styles.SpacerLabel}>Atau</Text>
              <View style={Styles.SpacerRight} />
            </View>
            {/* Mark: Media Login */}
            <View style={Styles.MediaLogin}>
              <TouchableOpacity style={Styles.MediaButton}>
                <Image source={FacebookLogo} style={Styles.MediaLogo} />
                <Text style={Styles.MediaLabel}>Facebook</Text>
              </TouchableOpacity>
              <TouchableOpacity style={Styles.MediaButton}>
                <Image source={GoogleLogo} style={Styles.MediaLogo} />
                <Text style={Styles.MediaLabel}>Google</Text>
              </TouchableOpacity>
            </View>
            <View style={Styles.ContainerBottomWord}>
              <Text >Belum punya akun?
                <Text >{" "}</Text>
                <Text style={Styles.NavtoRegister}>Yuk daftar</Text>
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  )
}
export default ViewIntro;

const Styles = StyleSheet.create({
  ContainerInner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F7F7F7'
  },
  LoginTextContainer: {
    width: "100%",
    paddingLeft: 20,
    justifyContent: 'flex-start',
    position: 'absolute',
  },
  LoginTextLabel: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#000',
    marginBottom: 20,
  },
  LoginTextSubLabel: {
    fontSize: 10,
    fontWeight: 'bold',
    color: '#000',
  },
  ImageContainer: {
    flex: .4,
    alignItems: 'center',
    justifyContent: 'flex-end',

  },
  MainLogoContainer: {
    justifyContent: 'center'
  },
  MainLogo: {
    resizeMode: "contain",
    height: "95%",
  },
  MainContainer: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#FFF',
    paddingTop: 20,
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40
  },
  ScrollContainer: {
  },
  WelcomeContainer: {
    flex: .5,
    width: "100%",
    paddingTop: 20
  },
  EmailInputContainer: {
    height: 100,
    width: "100%",
  },
  EmailInput: {
    height: 48,
    width: "90%",
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    borderColor: "#EEE",
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: 10
  },
  PasswordInputContent: {
    height: 48,
    width: "90%",
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    borderColor: "#EEE",
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    flexDirection: 'row'
  },
  InputLabel: {
    color: '#000',
    fontWeight: 'bold',
    marginLeft: 20
  },
  AvoidingKeyboard: {
    flex: 1
  },
  PasswordInputContainer: {
    height: 100,
    width: "100%",
  },
  PasswordInput: {
    height: 48,
    width: "80%",
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    alignSelf: 'center'
  },
  ForgotPassContainer: {
    width: "100%",
    alignItems: 'flex-end',
    paddingRight: 20
  },
  ForgotPassword: {
    color: '#000',
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    opacity: .5
  },
  ActionAuthButton: {
    width: "100%",
    marginTop: 50,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  RegisterButton: {
    height: 45,
    width: "90%",
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: '#009688'
  },
  RegisterText: {
    color: '#FFF',
    fontWeight: 'bold'
  },
  SpacerContainer: {
    marginVertical: 30,
    width: "100%",
    flexDirection: 'row',
    justifyContent: 'center',
  },
  SpacerLeft: {
    width: "40%",
    alignSelf: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#EEE'
  },
  SpacerLabel: {
    fontWeight: 'bold',
    paddingHorizontal: 20,
    color: '#EEE'
  },
  SpacerRight: {
    width: "40%",
    alignSelf: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#EEE'
  },
  FarewellContainer: {
    flex: .5,
    height: "100%",
    width: "100%",
    alignItems: 'center',
  },
  MediaLogin: {
    flexDirection: 'row',
    width: "100%",
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  MediaButton: {
    height: 50,
    width: "40%",
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    backgroundColor: '#EEE'
  },
  MediaLogo: {
    height: 12,
    width: 12,
    marginRight: 10,
  },
  MediaLabel: {
    color: '#000',
    fontWeight: 'bold'
  },
  NavtoRegister: {
    color: '#51B8A7',
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  },
  ContainerBottomWord: {
    marginTop: 30
  }
})
