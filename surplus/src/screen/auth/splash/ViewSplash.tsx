import { useNavigation } from "@react-navigation/native";
import React, { useEffect } from "react";
import { Image, SafeAreaView, StyleSheet, Text, View } from "react-native";

const MainLogo = require("../../../assets/MainLogo.png")
const CertifiedB = require("../../../assets/Certified-B.png")


const ViewSplash = () => {
    const Navigation = useNavigation()

    useEffect(() => {
        setTimeout(() => {
            Navigation.navigate("ViewIntro")
        }, 3000);
    })
    
    return (
        <SafeAreaView style={Styles.Container}>
            <View style={Styles.ContainerInner}>
                <View style={Styles.MainLogoContainer}>
                    <Image source={MainLogo} style={Styles.MainLogo} />
                    <Text style={Styles.Slogan}>Save food. Save budget</Text>
                    <Text style={Styles.Slogan}>Save planet</Text>
                </View>
                <View style={Styles.SloganContainer}>
                    <Image source={CertifiedB} style={Styles.MainLogo} />
                </View>
            </View>
        </SafeAreaView>
    )
}
export default ViewSplash;
const Styles = StyleSheet.create({
    Container: {
        flex: 1,
    },
    ContainerInner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    MainLogoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    MainLogo: {
        height: 120,
        width: 200
    },
    SloganContainer: {
        justifyContent: 'flex-end'
    },
    Slogan: {
        fontWeight: 'bold',
        color: '#51B8A7'
    }
})
