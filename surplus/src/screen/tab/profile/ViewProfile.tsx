import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import  {Buffer}  from "buffer";
import React, { useEffect, useState } from "react";
import { Image, ScrollView, StyleSheet, View, Text, TouchableOpacity } from "react-native";

const BlankImage = require("../../../assets/ProfileSection.png");
const PencilIcon = require("../../../assets/PencilIcon.png");
const FoodBag = require("../../../assets/FoodPaperBag.png");
const CopyLink = require("../../../assets/Copy.png");
const Discount = require("../../../assets/Discount.png");
const Comment = require("../../../assets/Comment.png");
const PieChart = require("../../../assets/PieChart.png");
const Community = require("../../../assets/Community.png");
const Partnership = require("../../../assets/Partnership.png");
const Rating = require("../../../assets/Rating.png");

const ViewProfile = () => {
    const Navigation = useNavigation()
    const [weather, setWeather] = useState([])
    const ContentList = [{
        id: 0,
        icon: Discount,
        title: "Voucher Saya",
        subtitle: `Lihat voucher kamu yang sedang aktif saat ini`
    },
    {
        id: 1,
        icon: Comment,
        title: "Ulasan makanan",
        subtitle: `Berikan ulasan kepada penjual makanan yang sudah kamu \nbeli`
    },
    {
        id: 2,
        icon: PieChart,
        title: "Dampak Infografik",
        subtitle: `Cari tahu seberapa besar makanan yang telah kamu \nselamatkan lewat aplikasi surplus`
    },
    {
        id: 3,
        icon: Community,
        title: "Komunitas Surplus",
        subtitle: `Bergabung dengan tim komunitas surplus untuk \nmemerangi masalah food waste di Indonesia`
    },
    {
        id: 4,
        icon: Partnership,
        title: "Surplus Partner",
        subtitle: `Perangi food waste dengan menjadi mitra kami lewat \naplikasi Surplus Partner`
    },
    {
        id: 5,
        icon: Rating,
        title: "Nilai Aplikasi Surplus",
        subtitle: `Berikan rating dan pengalaman kamu untuk aplikasi \nSurplus`
    },
    ];

    const LoopMenuList = (data: Array) => {
        return data.map((element: any) => (
            <TouchableOpacity style={Styles.LoopMenuContainer}>
                <Image source={element.icon} style={Styles.LoopImage} />
                <View style={Styles.LoopTitleContainer}>
                    <Text style={Styles.LoopTitle}>{element.title}</Text>
                    <Text style={Styles.LoopSubtitle}>{element.subtitle}</Text>
                </View>
            </TouchableOpacity>
        )
        )
    };

    useEffect(() => {
        getWeather()
    }, [])

    const getWeather = async () => {
        try {
            const res = await axios.get("https://www.7timer.info/bin/two.php?lon=106.7848&lat=6.2650&output=internal", { responseType: 'arraybuffer' });
            if (res) {
                const base64EncodedString = Buffer.from(res.data, 'binary').toString('base64');
                setWeather(base64EncodedString)
                // console.log('res', (res.data))

                // fileDownload(res.data, "filename")
                //   setLoading(false);
            }
        } catch (err: any) {
            console.log('errror home', { ...err });
        }

    }
    console.log('res weather', (weather))


    return (
        <View style={Styles.ContainerInner}>
            <ScrollView >
                {/* First Section */}
                <View style={Styles.FirstSection}>
                    <View style={Styles.ProfileImageContainer}>
                        <Image source={BlankImage} style={Styles.ImageProfile} />
                    </View>
                    <View style={Styles.BadgesWordingContainer}>
                        <Text style={Styles.BadgesWording}>Lakukan transaksi 1 kali untuk membuka badges</Text>
                    </View>
                        <Image source={{ uri: 'data:image/jpeg;base64,' + weather }} style={{ height: 250, width: 150, resizeMode: 'contain' }} />
                    <View style={Styles.UserNameContainer}>
                        <Text style={Styles.UsernameWording}>Kamal</Text>
                        <Image source={PencilIcon} style={Styles.PencilIcon} />
                    </View>
                    <Text style={Styles.EmailWording}>kamaldotpato@gmail.com</Text>
                    <Text style={Styles.EmailWording}>085159559233</Text>
                    <View style={Styles.ExplanationContainer}>
                        <View style={Styles.ExplanationContent}>
                            <Text style={Styles.ExplanationTitle}>{`Kamu \nmenyelamatkan`}</Text>
                            <Text style={Styles.ExplanationLabel}>0 makanan</Text>
                        </View>
                        <View style={Styles.ExplanationContent}>
                            <Text style={Styles.ExplanationTitle}>{`Total \npenghematan`}</Text>
                            <Text style={Styles.ExplanationLabel}>Rp. 0</Text>
                        </View>
                    </View>
                </View>

                <View style={Styles.Spacer} />

                {/* Second Section */}
                <View style={Styles.SecondSection}>
                    <View style={Styles.ReferalWording}>
                        <Image source={FoodBag} style={Styles.ReferalIcon} />
                        <View>
                            <Text style={Styles.ReferalTitle}>Kode Referal</Text>
                            <Text style={Styles.ReferalLabel}>
                                {`Yuk bagikan kode referal ke keluarga serta \ntemanmu! Dan dapatkan voucher makan sampai \ndengan 150 ribu loh!`}
                            </Text>
                            <Text style={Styles.ReferalLinked}>Pelajari lebih lanjut</Text>
                        </View>
                    </View>
                    <View style={Styles.CodeContainer}>
                        <Text style={Styles.ReferalContent}>k a m 8 1 5</Text>
                        <View style={Styles.CopyContainer}>
                            <Image source={CopyLink} style={Styles.CopyIcon} />
                            <Text style={Styles.CopyWording}>salin kode</Text>
                        </View>
                    </View>
                    <View style={Styles.ActionShareButton}>
                        <TouchableOpacity onPress={() => Navigation.navigate("TabRoute")} style={Styles.ShareButton}>
                            <Text style={Styles.ShareButtonText}>Bagikan Kode</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={Styles.Spacer} />

                {/* Third Section */}
                <View style={Styles.ThirdSection}>
                    {LoopMenuList(ContentList)}
                </View>
            </ScrollView>
        </View>
    )
}
export default ViewProfile;

const Styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    ContainerInner: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    FirstSection: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    ProfileImageContainer: {
        height: 110,
        width: 110,
        borderRadius: 100,
        borderWidth: 5,
        borderColor: "#EDEDED",
    },
    ImageProfile: {
        height: 100,
        width: 100,
        borderRadius: 100,
    },
    BadgesWordingContainer: {
        backgroundColor: "#F6FCFC",
        borderRadius: 10,
        paddingHorizontal: 5,
        paddingVertical: 2,
        marginVertical: 10,
        justifyContent: 'center'
    },
    UserNameContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    BadgesWording: {
        color: "#79B8A9",
        fontWeight: 'bold',
        fontSize: 11
    },
    UsernameWording: {
        color: "#000",
        fontWeight: 'bold',
        fontSize: 16
    },
    EmailWording: {
        color: "#939393",
        fontSize: 13
    },
    PencilIcon: {
        height: 20,
        width: 20,
        borderRadius: 100,
        backgroundColor: "green",
        marginLeft: 5
    },
    ExplanationContainer: {
        width: "100%",
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: 20,
    },
    ExplanationContent: {
        borderWidth: 1,
        borderColor: "#91CFC0",
        borderRadius: 10,
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    ExplanationTitle: {
        textAlign: 'center',
        color: '#91CFC0'
    },
    ExplanationLabel: {
        textAlign: 'center',
        color: '#0D9681',
        fontWeight: 'bold'
    },
    Spacer: {
        marginVertical: 20,
        borderBottomWidth: 3,
        borderBottomColor: "#F2F2F2",
        elevation: 2
    },
    SecondSection: {
        flex: 1,
    },
    ReferalWording: {
        flexDirection: 'row'
    },
    ReferalIcon: {
        height: 70,
        width: 70
    },
    ReferalTitle: {
        color: '#000',
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10
    },
    ReferalLabel: {
        color: '#A6A6A6',
        fontSize: 12,
    },
    ReferalLinked: {
        color: '#70C7B3',
        fontSize: 12,
        textDecorationLine: 'underline',
        fontWeight: 'bold',
        marginTop: 5
    },
    CodeContainer: {
        flexDirection: 'row',
        height: 40,
        width: "90%",
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        alignSelf: 'center',
        marginTop: 20,
        borderWidth: 1,
        borderRadius: 10,
        borderStyle: 'dashed',
        borderColor: "#91CFC0",
    },
    ReferalContent: {
        color: '#91CFC0',
        fontWeight: 'bold'
    },
    CopyWording: {
        color: '#91CFC0',
    },
    CopyContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    CopyIcon: {
        height: 20,
        width: 20
    },
    ActionShareButton: {
        width: "100%",
        marginTop: 20,
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    ShareButton: {
        height: 45,
        width: "90%",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: '#009688'
    },
    ShareButtonText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    ThirdSection: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 20
    },
    LoopMenuContainer: {
        flexDirection: 'row',
        width: "90%",
        marginTop: 10
    },
    LoopImage: {
        height: 20,
        width: 20,
        marginRight: 10
    },
    LoopTitleContainer: {

    },
    LoopTitle: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    LoopSubtitle: {
        marginTop: 10,
        fontSize: 12,
        color: "#A6A6A6"
    }
})
